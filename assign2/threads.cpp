/**
 * CSC456 - Operating Systems - Assignment 2
 * Thread Synchronization
 * Harshith Keni - 7327961
 * Instructor - Dr. Won
 */

#define BUF_SIZE 5

#include <iostream>
#include <string>
#include <queue>
#include <pthread.h>
using namespace std;

void * read(void *);
void * write(void *);
void * convert(void *);

// shared buffers
queue<string> bufA;
queue<string> bufB;
bool exitflag = false;

pthread_mutex_t lockA, lockB;

int main() {
    pthread_t r, c, w;
    cout << "Ctrol+C to exit" << endl;
    // init mutexes
    pthread_mutex_init(&lockA, NULL);
    pthread_mutex_init(&lockB, NULL);

    // create threads
    pthread_create(&r, NULL, read, NULL);
    pthread_create(&c, NULL, convert, NULL);
    pthread_create(&w, NULL, write, NULL);

    // wait for threads
    pthread_join(r, NULL);
    pthread_join(c, NULL);
    pthread_join(w, NULL);
    return 0;
}

// reader thread
void* read(void *) {
    while (!exitflag) {
        string s;
        getline(cin,s);
        if (s == "exit") {
            exitflag = true;
        }
        // lock a
        pthread_mutex_lock(&lockA);
        bufA.push(s);
        // unlock a
        pthread_mutex_unlock(&lockA);
    }
    pthread_exit(NULL);
}

// converter thread
void* convert(void *) {
    while (!exitflag) {
        if(!bufA.empty()) {
            // lock a
            pthread_mutex_lock(&lockA);
            string s = bufA.front();
            bufA.pop();
            //  unlock a
            pthread_mutex_unlock(&lockA);
            for(int i = 0; i < s.size(); i++) {
                if (s[i] == ' ') {
                    s[i] = '*';
                }
            }
            // lock b
            pthread_mutex_lock(&lockB);
            bufB.push(s);
            // unlock b
            pthread_mutex_unlock(&lockB);
        }
    }
    pthread_exit(NULL);
}

// writer threads
void* write(void *) {
    while (!exitflag) {
        if(!bufB.empty()) {
            // lock b
            pthread_mutex_lock(&lockB);
            cout << bufB.front() << endl;
            bufB.pop();
            // unlock b
            pthread_mutex_unlock(&lockB);
        }
    }
    pthread_exit(NULL);
}
