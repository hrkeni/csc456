#include <iostream>
#include <cstdio>
#include <algorithm>
#include "memory_manager.h"
#include "errors.h"
using namespace std;

int main() {
    setFIFO();
    cout << "What do you want to do? ('E' to exit)" << endl;
    string in;
    char op;
    int left, right;
    while (true) {
        getline(cin, in);
        transform(in.begin(), in.end(), in.begin(), ::toupper);
        if (in == "SET LRU") {
            setLRU();
            continue;
        } else if (in == "SET FIFO") {
            setFIFO();
            continue;
        } else if (in == "PRINT PF") {
            printPageFaults();
            continue;
        }
        sscanf(in.c_str(), "%c %d %d", &op, &left, &right);

        switch (op) {
            case 'M':
                try {
                    memoryManager(left, right);
                } catch (int e) {
                    cout << "Error initializing memory!" << endl;
                }
                break;
            case 'P':
                try {
                    printMemory();
                } catch (int e) {
                    cout << "Error printing memory!" << endl;
                }
                break;
            case 'A': {
                int s = allocate(left, right);
                if (s == 1) {
                    cout << "Error allocating memory!" << endl;
                } else {
                    cout << "Allocation success!" << endl;
                }
                break;
            }
            case 'D': {
                int s = deallocate(left);
                if (s == -1) {
                    cout << "Error deallocating memory!" << endl;
                } else {
                    cout << "Deallocation success!" << endl;
                }
                break;
            }
            case 'W': {
                int s = write(left, right);
                if (s == -1) {
                    cout << "Error writing memory!" << endl;
                } else {
                    cout << "Write success!" << endl;
                }
                break;
            }
            case 'R': {
                int s = read(left, right);
                if (s == -1) {
                    cout << "Error writing memory!" << endl;
                } else {
                    cout << "Read success! Value: " << s << endl;
                }
                break;
            }
            case 'E':
                cout << "Exiting!" << endl;
                exit(0);
            default:
                cout << "Unrecognized command!" << endl;
        }
    }

    // printMemory();
    return 0;
}
