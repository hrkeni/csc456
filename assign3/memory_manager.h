#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

struct process {
    int pid;
    int size;
    int *frames; //page table - maps to frames
};


void memoryManager(int bytes, int frameSize);
int allocate(int bytes, int pid);
int deallocate(int pid);
int write(int pid, int logicalAddress);
int read(int pid, int logicalAddress);
void printMemory(void);
void setLRU();
void setFIFO();
void printPageFaults();


#endif /* end of include guard: MEMORY_MANAGER_H */
