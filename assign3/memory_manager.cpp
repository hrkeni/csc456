#include <iostream>
#include <cstdint>
#include <stdlib.h>
#include <list>
#include "memory_manager.h"
#include "errors.h"
#include <sys/time.h>

using namespace std;
typedef uint8_t byte;

byte * memory = NULL;
int memorySize;
int frameSize;
int availableSize;
int noOfFrames;
int * frameList;
long * frameTime;
list<int> lruQueue;
list<process> processList;
bool isLRU = true;
int pageFaults = 0;

/**
 * Prints the number of page faults!
 */
void printPageFaults() {
    cout << "Page faults: " << pageFaults << endl;
}

/**
 * Sets the page replacement algorithm to FIFO
 */
void setFIFO() {
    cout << "Page replacement algorithm set to FIFO!" << endl;
    isLRU = false;
}

/**
 * Sets the page replacement algorithm to LRU
 */
void setLRU() {
    cout << "Page replacement algorithm set to LRU!" << endl;
    isLRU = true;
}

/**
 * Allocates a chunk of memory
 * @throws SIZE_ERROR - if bytes is not a multiple of frameSize
 * @throws ALLOCATION_ERROR - if memory already allocated
 * @param bytes(int) - the number of bytes
 * @param frameSize(int) - the size of frames
 */
void memoryManager(int bytes, int frameSize) {
    if (bytes % frameSize != 0) {
        std::cerr << "bytes must be a multiple of frameSize" << std::endl;
        throw SIZE_ERROR;
    }
    if (memory != NULL) {
        std::cerr << "memory already allocated" << std::endl;
        throw ALLOCATION_ERROR;
    }
    memory = (byte *) calloc (bytes, sizeof(byte));
    memorySize = bytes;
    availableSize = memorySize;
    ::frameSize = frameSize;
    noOfFrames = memorySize / frameSize;

    frameList = new int[noOfFrames];
    frameTime = new long[noOfFrames];
    for (int i = 0; i < noOfFrames; i++) {
        frameList[i] = -1;
    }
}

/**
 * Swaps out a page
 * @param index (int)
 */
void swap(int index) {
    cout << "Page fault! Swapped out frame " << index <<
        " belonging to process " << frameList[index] << endl;
    frameList[index] = -1;
    frameTime[index] = 0;
    lruQueue.remove(index);
    availableSize += frameSize;
    pageFaults++;
}

/**
 * Swaps using LRU
 */
void swapLRU() {
    int index = lruQueue.front();
    lruQueue.pop_front();
    swap(index);
}

/**
 * Swaps using FIFO
 */
void swapFIFO() {
    int oldest = 0;
    for (int i = 1; i < noOfFrames; i++) {
        if (frameTime[oldest] > frameTime[i]) {
            oldest = i;
        }
    }
    swap(oldest);
}

/**
 * Allocates frames
 * @param pid (int) the pid
 * @param num (int) number of frames
 */
void allocateFrames(int pid, int num) {
    process p;
    p.pid = pid;
    p.size = num;
    p.frames = new int[num];
    for (int c = 0; c < num; ) {
        int i = rand() % noOfFrames;
        if (availableSize <= 0) {
            if (isLRU) {
                swapLRU();
            } else {
                swapFIFO();
            }
        }
        if (frameList[i] == -1) {
            p.frames[c] = i;
            frameList[i] = pid;
            c++;
            availableSize -= frameSize;

            // set time for frame
            struct timeval tp;
            gettimeofday(&tp, NULL);
            long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
            frameTime[i] = ms;

            // push to lru queue
            lruQueue.push_back(i);
        }
    }
    processList.push_back(p);
}

/**
 * Allocates frames
 * @param bytes (int) size of memory
 * @param pid (int) the pid
 * @return    (int) 0 if success, 1 if not
 */
int allocate(int bytes, int pid) {
    if (memory == NULL) {
        std::cerr << "Memory not initialized" << std::endl;
        return 1;
    }

    int num;
    num = (bytes + frameSize - 1) / frameSize;  // always round up
    allocateFrames(pid, num);

    return 0;
}

/**
 * Deallocates frames
 * @param pid (int) the pid
 * @return    (int) 1 if success, -1 if not
 */
int deallocate(int pid) {
    if (memory == NULL) {
        std::cerr << "Memory not initialized" << std::endl;
        return -1;
    }
    int c = 0;
    for (int i = 0; i < noOfFrames; i++) {
        if (frameList[i] == pid) {
            frameList[i] = -1;
            c++;
        }
    }
    for (list<process>::iterator it = processList.begin();
            it != processList.end(); it++) {
        process p = *it;
        if(p.pid == pid) {
            delete p.frames;
            processList.erase(it);
            break;
        }
    }

    availableSize += c * frameSize;
    return 1;
}

/**
 * Prints contents of memory
 */
void printMemory() {
    if (memory == NULL) {
        std::cerr << "Memory not initialized" << std::endl;
        throw ALLOCATION_ERROR;
    }
    cout << "Memory (Frame: pid if allocated, -1 if free, Memory: value in memory):" << endl;
    for (int i = 0; i < memorySize; i++ ) {
        cout << "Frame: " << frameList[i/frameSize] << " Memory: "
             << (int)*(memory+i) << " Time: "<< frameTime[i/frameSize] << endl;
    }
    cout << "Process list: " << endl;
    for (list<process>::iterator it = processList.begin();
            it != processList.end(); it++) {
        process p = *it;
        cout << "pid: " << p.pid << " Size: " << p.size << endl;
    }
    cout << "LRU queue: " << endl;
    for (list<int>::iterator it = lruQueue.begin();
            it != lruQueue.end(); it++) {
        int i = *it;
        cout << i << " ";
    }
    cout << endl;
}

/**
 * Writes '1' to memory
 * @param  pid            (int) pid
 * @param  logicalAddress (int) the logical address
 * @return                (int) 1 if success, -1 if not
 */
int write(int pid, int logicalAddress) {
    if (memory == NULL) {
        std::cerr << "Memory not initialized" << std::endl;
        return -1;
    }
    int i, c;
    for (i = 0, c = 0; c < logicalAddress && i < noOfFrames; i++) {
        if ( i > noOfFrames) {
            std::cerr << "Invalid pid or logical address" << std::endl;
            return -1;
        }
        if (frameList[i] == pid) {
            c++;
        }
    }
    if (frameList[i] != pid) {
        std::cerr << "Invalid logical address" << std::endl;
        return -1;
    }
    *(memory+i) = 1;
    return 1;
}

/**
 * reads from memory
 * @param  pid            (int) pid
 * @param  logicalAddress (int) the logical address
 * @return                (int) value if success, -1 if not
 */
int read(int pid, int logicalAddress) {
    if (memory == NULL) {
        std::cerr << "Memory not initialized" << std::endl;
        return -1;
    }
    int i, c;
    for (i = 0, c = 0; c < logicalAddress && i < noOfFrames; i++) {
        if ( i > noOfFrames) {
            std::cerr << "Invalid pid or logical address" << std::endl;
            return -1;
        }
        if (frameList[i] == pid) {
            c++;
        }
    }
    if (frameList[i] != pid) {
        std::cerr << "Invalid logical address" << std::endl;
        return -1;
    }
    return *(memory+i);
}
