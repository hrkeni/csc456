/**
 * CSC456 - Operating Systems - Assignment 1
 * Simple Shell
 * Harshith Keni - 7327961
 * Instructor - Dr. Won
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define MAX_SIZE 1024
#define true 1
#define false 0
#define WHITESPACE " \t\r\n"

int main() {
    char line[MAX_SIZE];
    char ** args = NULL;
    char * saveptr;
    pid_t pid;
    int argc;
    printf("Welcome to keni shell!\n");
    while (true) {
        errno = 0;
        argc = 0;
        printf("keni$ ");

        // read a line
        if (!fgets(line, MAX_SIZE, stdin)) {
            break;
        }

        //parse the line - put each arg into an array of strings
        saveptr = strtok(line, WHITESPACE);
        while (saveptr) {
            args = realloc(args, sizeof (char *) * ++argc);
            if (args == NULL) {
                fprintf(stderr, "Allocation failed!\n" );
                exit(-1);
            }
            args[argc-1] = saveptr;
            saveptr = strtok(NULL, WHITESPACE);
        }
        // make sure array is null terminated
        args = realloc(args, sizeof (char *) * (argc+1));
        args[argc] = NULL;

        // exit command
        if (strcmp(args[0], "exit") == 0) {
            printf("Goodbye!\n");
            break;
        } else {
            // spawn a child process
            pid = fork();
            if (pid < 0) {  // error forking
                fprintf(stderr, "Fork failed!");
                return 1;
            } else if (pid == 0) {  // child process
                execvp(args[0], args);
            } else {  // parent process
                wait(NULL);
            }
        }
        if (errno) {  // print error message
            perror("Error");
        }
    }
    return 0;
}
